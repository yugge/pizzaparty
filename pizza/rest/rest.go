package rest

import (
	"fmt"
	"encoding/json"
	"net/http"
	"bitbucket.org/yugge/pizzaparty/pizza/api"
	"io/ioutil"
)

const (	
	apiURL = "https://onlinepizza.se/api/1.2/rest?"
	country = "SE"
	zipcode = "97752"
)

type ATTR struct {
	Id		string
	Name 	string
}

type OPINION struct {
	Delivery string
	Food_quality string
	General string
}

type RESTAURANT struct {
	Attributes ATTR `json:"@attributes"`
	Address string
	Zipcode string
	City string
	Country_code string
	Opinion OPINION
	Warning_message string
	Online string
}

type RESTAURANTS struct {
	Restaurant []RESTAURANT
}

type RESPONSE struct {
	Restaurants RESTAURANTS
}

func GetResturants(key api.APIKEY, session_key string){
	var r RESPONSE

	call_Apikey  := "api_key=" + key.Public
	call_Country := "country_code=" + country 
	call_Method  := "method=library.getRestaurantsByCountryCodeAndZipCode"
	call_Session := "session_key=" + session_key
	call_Zip := "zipcode=" + zipcode

	sig_text := call_Apikey
	sig_text += call_Country
	sig_text += call_Method
	sig_text += call_Session
	sig_text += call_Zip
	sig_text += key.Secret
	sig := api.GetMD5sum(sig_text)

	response, _ := http.Get(apiURL+call_Method+"&"+call_Apikey+"&"+call_Zip+"&"+call_Country+"&"+call_Session+"&sig="+sig)
	js, _ := ioutil.ReadAll(response.Body)
	err := json.Unmarshal(js, &r)
	if err != nil{
		fmt.Println(err)
	}
	fmt.Println(r.Restaurants.Restaurant[0])
}