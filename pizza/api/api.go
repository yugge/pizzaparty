package api

import (
	"fmt"
	"os"
	"io"
	"io/ioutil"
	"encoding/json"
	"crypto/md5"
	)

const (
	apiFileName = "api.txt"
)

type APIKEY struct {
	Public string
	Secret string
}

func GetAPIKeys()APIKEY{
	var key APIKEY

	workingDir,_ := os.Getwd()
	fileRAW,_ := os.OpenFile(workingDir + "/" + apiFileName, os.O_RDWR, 0777)
	fileBytes,_ := ioutil.ReadAll(fileRAW)
	
	err := json.Unmarshal(fileBytes,&key)
	if err != nil {
		fmt.Printf("Error loading api keys. Is api.txt there?")
		panic("API Error")
	}

	return key
}

func GetMD5sum(text string)string{
	h := md5.New()
	io.WriteString(h,text)
	sig := fmt.Sprintf("%x",h.Sum(nil))
	return sig
}