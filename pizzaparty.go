package main

import (
	"fmt"
	"io/ioutil"
	"encoding/json"
	"net/http"
	"bitbucket.org/yugge/pizzaparty/pizza/api"
	"bitbucket.org/yugge/pizzaparty/pizza/rest"
)

const (	
	apiURL = "https://onlinepizza.se/api/1.2/rest?"
)

type SESSIONKEY struct {
	Session_key string
}


func getSessionKey(key api.APIKEY)string{
	var r SESSIONKEY

	call_Method := "method=auth.getVoidSession"
	call_Apikey := "api_key="+key.Public

	sig_text := call_Apikey
	sig_text += call_Method
	sig_text += key.Secret
	sig := api.GetMD5sum(sig_text)

	response, _ := http.Get(apiURL+call_Method+"&"+call_Apikey+"&sig="+sig)
	js, _ := ioutil.ReadAll(response.Body)
	err := json.Unmarshal(js,&r)
	if err != nil {
		fmt.Println(err)
	}
	return r.Session_key
}



func main(){
	var key api.APIKEY

	key = api.GetAPIKeys()
	session_key := getSessionKey(key)
	fmt.Println(session_key)
	rest.GetResturants(key,session_key)

}